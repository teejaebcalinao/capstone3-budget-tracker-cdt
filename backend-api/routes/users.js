const router = require('express').Router()
const {emailExists,categoryExists} = require('../validation')
const { verifyToken } = require('../auth')

const userController = require('../controllers/userController')

console.log()

//post users

router.post('/register',emailExists, userController.registerUser)

router.post('/login', userController.loginUser)

//post categories

router.post('/add-category', verifyToken, categoryExists, userController.addCategory)

//post entries
router.post('/add-entry', verifyToken, userController.addEntry)

router.get('/', verifyToken, userController.userDetails)
router.get('/get-expense-entries', verifyToken, userController.getExpenseEntries)
router.get('/get-income-entries', verifyToken, userController.getIncomeEntries)
router.get('/get-categories/Expense', verifyToken, userController.getExpenseCategories)
router.get('/get-categories/Income', verifyToken, userController.getIncomeCategories)

module.exports = router