const User = require('./../models/User')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const {createAccessToken} = require("../auth.js")

module.exports.registerUser = (req,res) => {

	const hashedPw = bcrypt.hashSync(req.body.password, 10)

	let newUser = new User({

			name: req.body.name,
			email: req.body.email,
			password: hashedPw

	})
	
	newUser.save()
	.then(user => {
		res.send(user)
	})
	.catch(err => {
		res.send(err)
	})


}



module.exports.loginUser = (req,res) => {

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			res.send({error: "Email is not registered."})
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			if(isPasswordCorrect){
				res.send({accessToken: createAccessToken(foundUser)})
			} else {

				res.send({error: "Password is incorrect."})
			}

		}

	})
	.catch(error => {
		res.send(error)
	})

}

//get user details
module.exports.userDetails = (req,res) => {

	User.findById(req.user.id, {password: 0})
	.then(foundUser => {
		res.send(foundUser)
	})
	.catch(error => {
		res.send(error)
	})

}

module.exports.addCategory = (req,res) => {

	console.log(req.body)

	let newCategory = {

		name: req.body.name,
		type: req.body.type

	}

	User.findById(req.user.id)
	.then(foundUser => {

		foundUser.categories.push(newCategory)
		foundUser.save()
		.then(user => {

			console.log(user.categories[user.categories.length-1])
			res.send({isAdded: true})
		})

	})
	.catch(err => {
		res.send(err)
	})


}

module.exports.getExpenseCategories = (req,res) => {

	User.findById(req.user.id)
	.then(user => {
		let expenses = user.categories.filter(category=>category.type === "Expense")
		let expenseCategories = expenses.map(expense => expense.name)
		res.send(expenseCategories)
	})
	.catch(err => {
		res.send(err)
	})

}
module.exports.getIncomeCategories = (req,res) => {

	User.findById(req.user.id)
	.then(user => {
		let income = user.categories.filter(category=>category.type === "Income")
		let incomeCategories = income.map(expense => expense.name)
		res.send(incomeCategories)
	})
	.catch(err => {
		res.send(err)
	})
}



module.exports.addEntry = (req,res) => {

	console.log(req.body)

	let newEntry = {

		description: req.body.description,
		category: req.body.category,
		type: req.body.type,
		amount: req.body.amount

	}

	User.findById(req.user.id)
	.then(foundUser => {

		foundUser.entries.push(newEntry)
		foundUser.save()
		.then(user => {

			console.log(user.entries[user.entries.length-1])
			res.send({isAdded: true})
		})

	})
	.catch(err => {
		res.send(err)
	})


}

module.exports.getExpenseEntries = (req,res) => {

	User.findById(req.user.id)
	.then(user => {
		let expenses = user.entries.filter(entry=>entry.type === "Expense")
		res.send(expenses)
	})
	.catch(err => {
		res.send(err)
	})

}
module.exports.getIncomeEntries = (req,res) => {

	User.findById(req.user.id)
	.then(user => {
		let income = user.entries.filter(entry=>entry.type === "Income")
		res.send(income)
	})
	.catch(err => {
		res.send(err)
	})
}