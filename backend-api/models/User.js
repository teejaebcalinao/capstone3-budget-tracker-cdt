const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
     name: {
        type: String,
        required: [ true, "Name required"]
    },
    email: {
        type: String,
        required: [ true, "Email required"],
        unique: true
    },
    password: {
        type: String,
        required: [ true, "Password required"]
    },
    categories: [
        {
            name: {
                type: String,
                required: [ true, "Category name required"]
            },
            type: {
                type: String,
                require: [ true, "Category type required"],
            }
        }
    ],
    entries: [
        {

            description: {
                type: String,
                required: [true, "Description required"]
            },
            category: {
                type: String,
                required: [true, "Category name required"]
            },
            amount: {
                type: Number,
                required: [true, "Amount required"]
            },
            type: {
                type: String,
                require: [ true, "Entry type required"],
            },
            dateAdded: {
                type: Date,
                default: new Date()
            }

        }
    ]
})

module.exports = mongoose.model('User', UserSchema)