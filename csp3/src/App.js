// Base Imports
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// App Imports
import UserContext from 'UserContext';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Login from 'pages/Login';
import AddEntry from 'pages/AddEntry';
import AddCategory from 'pages/AddCategory';
import ViewExpense from 'pages/ViewExpense';
import ViewIncome from 'pages/ViewIncome';
import Register from 'pages/Register';
import NotFound from 'pages/NotFound';

export default function App() {
    const [user, setUser] = useState({ accessToken: localStorage.getItem('accessToken') });

    const unsetUser = () => {
        localStorage.clear();
        setUser({ accessToken: null });
    }
    
    return (
        <UserContext.Provider value={{ user, setUser, unsetUser }}>
            <BrowserRouter>
                <AppNavbar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/add-entry" component={AddEntry}/>
                    <Route exact path="/add-category" component={AddCategory}/>
                    <Route exact path="/view-expense" component={ViewExpense}/>
                    <Route exact path="/view-income" component={ViewIncome}/>
                    <Route component={NotFound}/>
                </Switch>
            </BrowserRouter>
        </UserContext.Provider>
    )
}