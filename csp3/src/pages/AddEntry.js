// Base Imports
import React, { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function AddEntry() {
    const [type, setType] = useState('Income');
    const [category, setCategory] = useState(undefined);
    const [description, setDescription] = useState('');
    const [amount, setAmount] = useState(0);
    const [categories, setCategories] = useState(null);
    const { user } = useContext(UserContext);

    function getCategories() {
        fetch('http://localhost:4000/users/get-categories/'+type, {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            }
        })
        .then((response) => response.json())
        .then((response) => {
            const categoryOptions = response.map((category) => <option>{category}</option>);
            setCategories(categoryOptions);
        });
    }

    function addEntry(e) {
        e.preventDefault();
        fetch('http://localhost:4000/users/add-entry', {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            },
            body: JSON.stringify({ 
                category: category, 
                type: type,
                description: description,
                amount: amount
            })
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.isAdded === true) {
                alert('New entry added.');
                setCategory(undefined);
                setType('Income');
                setDescription('');
                setAmount(0);
            } else {
                alert(response.error);
            }
        })
    }

    useEffect(getCategories, [type]);

    if (user.accessToken === null) {
        return <Redirect to="/login"/>;
    }

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={4}>
                    <h3>Add Entry</h3>
                    <Form onSubmit={addEntry}>
                        <Form.Group>
                            <Form.Label>Type</Form.Label>
                            <Form.Control as="select" value={type} onChange={(e) => setType(e.target.value)} required>
                                <option>Income</option>
                                <option>Expense</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" value={category} onChange={(e) => setCategory(e.target.value)} required>
                                <option selected value disabled>Select Category</option>
                                {categories}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Amount</Form.Label>
                            <Form.Control type="number" value={amount} onChange={(e) => setAmount(e.target.value)} required/>
                        </Form.Group>
                        <Button variant="success" type="submit">Add Entry</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}