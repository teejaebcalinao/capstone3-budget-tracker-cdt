// Base Imports
import React, { useContext, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';

export default function ViewExpense() {
    const [expenses, setExpenses] = useState([]);
    const { user } = useContext(UserContext);

    function getExpenseEntries() {
        fetch('http://localhost:4000/users/get-expense-entries', {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            }
        })
        .then((response) => response.json())
        .then((response) => {
            console.log(response)
            setExpenses(response);
        });
    }

    useEffect(getExpenseEntries, []);

    if (user.accessToken === null) {
        return <Redirect to="/login"/>;
    }

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={8}>
                    <h3>View Expense Entries</h3>
                    <Table striped bordered hover responsive>
                        <thead>
                            <tr>
                                <th>Datetime</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                expenses.map((expense) => (
                                    <tr key={expense._id}>
                                        <td>{expense.dateAdded}</td>
                                        <td>{expense.description}</td>
                                        <td>{expense.category}</td>
                                        <td>{expense.amount}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    )
}