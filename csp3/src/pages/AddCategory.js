// Base Imports
import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function AddCategory() {
    const [name, setName] = useState('');
    const [type, setType] = useState('Income');
    const { user } = useContext(UserContext);

    function addCategory(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/add-category', {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            },
            body: JSON.stringify({ name: name, type: type })
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.isAdded === true) {
                alert('New category added.');
                setName('');
                setType('');
            } else {
                alert(response.error);
            }
        });
    }

    if (user.accessToken === null) {
        return <Redirect to="/login"/>;
    }

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={4}>
                    <h3>Add Category</h3>
                    <Form onSubmit={addCategory}>
                        <Form.Group>
                            <Form.Label>Category Name</Form.Label>
                            <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Type</Form.Label>
                            <Form.Control as="select" value={type} onChange={(e) => setType(e.target.value)} required>
                                <option>Income</option>
                                <option>Expense</option>
                            </Form.Control>
                        </Form.Group>
                        <Button variant="success" type="submit">Add Category</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}