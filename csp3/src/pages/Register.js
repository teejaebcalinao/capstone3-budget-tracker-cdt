// Base Imports
import React, { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Register() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    const { user } = useContext(UserContext);
    const history = useHistory();

    useEffect(() => {
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';
        let isPasswordConfirmNotEmpty = passwordConfirm !== '';
        let isPasswordMatched = password === passwordConfirm;

        // Determine if all conditions have been met.
        if (isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatched) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
    }, [email, password, passwordConfirm]);
    
    function register(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/register', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name: name,
                email: email, 
                password: password 
            })
        })
        .then((response) => response.json())
        .then((response) => {
            console.log(response)
            if (response._id !== undefined) {
                alert('Registration successful, you may now log in.');
                history.push('/login');
            } else {
                alert(response.error);
            }
        });
    }    

    if (user.accessToken !== null) {
        return <Redirect to="/"/>;
    }

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={4}>
                    <h3>Register</h3>
                    <Form onSubmit={register}>
                        <Form.Group>
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" value={name} onChange={(e) => setName(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control type="password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required/>
                        </Form.Group>
                        <Button variant="success" type="submit" disabled={isDisabled}>Register</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}