// Base Imports
import React from 'react';
import { Link } from 'react-router-dom';

// Bootstrap Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

export default function NotFound() {
    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={4}>
                    <h3>Page Not Found</h3>
                    <p>Go back to the <Link to="/">homepage</Link>.</p>
                </Col>
            </Row>
        </Container>
    )
}