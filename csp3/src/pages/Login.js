// Base Imports
import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { user, setUser } = useContext(UserContext);

    function login(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email: email, password: password })
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.accessToken !== undefined) {
                localStorage.setItem('accessToken', response.accessToken);
                setUser({ accessToken: response.accessToken });
            } else {
                alert(response.error);
                setEmail('');
                setPassword('');
            }
        });
    }

    if (user.accessToken !== null) {
        return <Redirect to="/"/>;
    }

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={4}>
                    <h3>Login</h3>
                    <Form onSubmit={login}>
                        <Form.Group>
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                        </Form.Group>
                        <Button variant="success" type="submit">Login</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}