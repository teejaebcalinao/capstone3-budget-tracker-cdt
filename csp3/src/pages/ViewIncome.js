// Base Imports
import React, { useContext, useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

// App Imports
import UserContext from 'UserContext';

// Bootstrap Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';

export default function ViewExpense() {
    const [incomes, setIncomes] = useState([]);
    const { user } = useContext(UserContext);

    function getIncomeEntries() {
        fetch('http://localhost:4000/users/get-income-entries', {
            method: 'GET',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.accessToken}`
            }
        })
        .then((response) => response.json())
        .then((response) => {
            setIncomes(response);
        });
    }

    useEffect(getIncomeEntries, []);

    if (user.accessToken === null) {
        return <Redirect to="/login"/>;
    }

    return (
        <Container fluid>
            <Row className="justify-content-center">
                <Col sm={12} md={8}>
                    <h3>View Income Entries</h3>
                    <Table striped bordered hover responsive>
                        <thead>
                            <tr>
                                <th>Datetime</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                incomes.map((income) => (
                                    <tr key={income._id}>
                                        <td>{income.dateAdded}</td>
                                        <td>{income.description}</td>
                                        <td>{income.category}</td>
                                        <td>{income.amount}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    )
}